package com.adarsh.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.ManagePets;

import java.util.ArrayList;

public class PetsMasterAdapter extends RecyclerView.Adapter<PetsMasterAdapter.MyViewHolder> {

    ArrayList<String> NameList, IdList;
    Context mContext;

    public PetsMasterAdapter(Context context, ArrayList<String> NameList1, ArrayList<String> IdList1) {

        this.mContext = context;

        NameList = new ArrayList<>();
        IdList = new ArrayList<>();

        this.NameList = NameList1;
        this.IdList = IdList1;

    }

    @Override
    public PetsMasterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapterforpetsmaster, parent, false);

        return new PetsMasterAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PetsMasterAdapter.MyViewHolder holder, final int position) {

        holder.petsName.setText(NameList.get(position));


        holder.editpetbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(mContext, ManagePets.class);
                mContext.startActivity(intent1);
                //((Activity) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return NameList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView petsName;
        ImageView editpetbtn;

        public MyViewHolder(View view) {
            super(view);

            petsName = (TextView) view.findViewById(R.id.tvName_petsAdapter);
            editpetbtn = (ImageView)view.findViewById(R.id.imgedit_petsAdapter);

        }
    }
}

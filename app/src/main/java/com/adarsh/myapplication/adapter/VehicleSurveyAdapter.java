package com.adarsh.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.ManagePets;

import java.util.ArrayList;

public class VehicleSurveyAdapter extends RecyclerView.Adapter<VehicleSurveyAdapter.MyViewHolder> {

    ArrayList<String> VehicleName,VehicleType,OwnerName,ModelNo,ShipMode,Insurance, IdList;
    Context mContext;

    public VehicleSurveyAdapter(ArrayList<String> vehicleName, ArrayList<String> vehicleType, ArrayList<String> ownerName,
                                ArrayList<String> modelNo, ArrayList<String> shipMode, ArrayList<String> insurance,
                                ArrayList<String> idList, Context mContext) {
        this.VehicleName = vehicleName;
        this.VehicleType = vehicleType;
        this.OwnerName = ownerName;
        this.ModelNo = modelNo;
        this.ShipMode = shipMode;
        this.Insurance = insurance;
        this.IdList = idList;
        this.mContext = mContext;
    }

    @Override
    public VehicleSurveyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vehiclesurveyadapter, parent, false);

        return new VehicleSurveyAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VehicleSurveyAdapter.MyViewHolder holder, final int position) {

        holder.vehicleName.setText(VehicleName.get(position));
        holder.vehicleType.setText(VehicleType.get(position));
        holder.ownerName.setText(OwnerName.get(position));
        holder.modelNo.setText(ModelNo.get(position));
        holder.shipMode.setText(ShipMode.get(position));
        holder.insurance.setText(Insurance.get(position));


        holder.editVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(mContext, ManagePets.class);
                mContext.startActivity(intent1);
                //((Activity) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return VehicleName.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView vehicleName,vehicleType,ownerName,modelNo,shipMode,insurance;
        ImageView editVehicle;

        public MyViewHolder(View view) {
            super(view);

            vehicleName = (TextView) view.findViewById(R.id.vehicleName_VehicleSurveyAdapter);
            vehicleType = (TextView) view.findViewById(R.id.vehicleType_VehicleSurveyAdapter);
            ownerName = (TextView) view.findViewById(R.id.ownerName_VehicleSurveyAdapter);
            modelNo = (TextView) view.findViewById(R.id.modelNo_VehicleSurveyAdapter);
            shipMode = (TextView) view.findViewById(R.id.shipMode_VehicleSurveyAdapter);
            insurance = (TextView) view.findViewById(R.id.insurance_VehicleSurveyAdapter);
            editVehicle = (ImageView)view.findViewById(R.id.editVehicle_VehicleSurveyAdapter);

        }
    }
}

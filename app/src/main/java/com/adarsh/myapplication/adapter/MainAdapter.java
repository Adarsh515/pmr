package com.adarsh.myapplication.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.NewSurvey;

import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MyViewHolder> {

    ArrayList<String> NameList, IDList, SourceList, DestinationList, DateList, TimeList;
    Context mContext;

    public MainAdapter(Context context, ArrayList<String> NameList1, ArrayList<String> IDList1, ArrayList<String> SourceList1,
                       ArrayList<String> DestinationList1, ArrayList<String> DateList1, ArrayList<String> TimeList1) {

        this.mContext = context;

        NameList = new ArrayList<>();
        DestinationList = new ArrayList<>();
        DateList = new ArrayList<>();
        SourceList = new ArrayList<>();
        TimeList = new ArrayList<>();
        IDList = new ArrayList<>();

        this.NameList = NameList1;
        this.DestinationList = DestinationList1;
        this.DateList = DateList1;
        this.SourceList = SourceList1;
        this.TimeList = TimeList1;
        this.IDList = IDList1;

    }

    @Override
    public MainAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.survey_home_adapter, parent, false);

        return new MainAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MainAdapter.MyViewHolder holder, final int position) {

        holder.NameTv.setText(NameList.get(position));
        holder.SourceTv.setText(SourceList.get(position));
        holder.Destination1Tv.setText(DestinationList.get(position));
        //holder.Destination2Tv.setText(NameList.get(position));
        holder.DateTv.setText(DateList.get(position));
        holder.TimeTv.setText(TimeList.get(position));

    }

    @Override
    public int getItemCount() {
        return NameList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView NameTv, SourceTv, Destination1Tv, Destination2Tv, DateTv, TimeTv;
        RelativeLayout linearLayout;


        public MyViewHolder(View view) {
            super(view);

            NameTv = (TextView) view.findViewById(R.id.name_mainAdapter);
            SourceTv = (TextView) view.findViewById(R.id.source_mainAdapter);
            Destination1Tv = (TextView) view.findViewById(R.id.destination1_mainAdapter);
            Destination2Tv = (TextView) view.findViewById(R.id.destination2_mainAdapter);
            DateTv = (TextView) view.findViewById(R.id.date_mainAdapter);
            TimeTv = (TextView) view.findViewById(R.id.time_mainAdapter);
            linearLayout = (RelativeLayout) view.findViewById(R.id.linear_mainAdapter);

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentMainAdapter = new Intent(mContext, NewSurvey.class);
                    mContext.startActivity(intentMainAdapter);
                }
            });

        }
    }
}

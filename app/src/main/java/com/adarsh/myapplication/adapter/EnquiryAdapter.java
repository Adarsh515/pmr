package com.adarsh.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.NewSurvey;

import java.util.ArrayList;

/**
 * Created by Adarsh on 03-12-2018.
 */

public class EnquiryAdapter extends RecyclerView.Adapter<EnquiryAdapter.MyViewHolder> {

    ArrayList<String> NameList, IDList, SourceList, DestinationList, DateList, TimeList;
    Context mContext;

    public EnquiryAdapter(Context context, ArrayList<String> NameList1, ArrayList<String> IDList1, ArrayList<String> SourceList1,
                          ArrayList<String> DestinationList1, ArrayList<String> DateList1, ArrayList<String> TimeList1) {

        this.mContext = context;

        NameList = new ArrayList<>();
        DestinationList = new ArrayList<>();
        DateList = new ArrayList<>();
        SourceList = new ArrayList<>();
        TimeList = new ArrayList<>();
        IDList = new ArrayList<>();

        this.NameList = NameList1;
        this.DestinationList = DestinationList1;
        this.DateList = DateList1;
        this.SourceList = SourceList1;
        this.TimeList = TimeList1;
        this.IDList = IDList1;

    }

    @Override
    public EnquiryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.enquiry_home_adapter, parent, false);

        return new EnquiryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EnquiryAdapter.MyViewHolder holder, final int position) {

        holder.NameTv.setText(NameList.get(position));
        holder.SourceTv.setText(SourceList.get(position));
        holder.Destination1Tv.setText(DestinationList.get(position));
        //holder.Destination2Tv.setText(NameList.get(position));
        holder.DateTv.setText(DateList.get(position));
        holder.TimeTv.setText(TimeList.get(position));

    }

    @Override
    public int getItemCount() {
        return NameList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView NameTv, SourceTv, Destination1Tv, Destination2Tv, DateTv, TimeTv;
        Button StartSurveyBtn;


        public MyViewHolder(View view) {
            super(view);

            NameTv = (TextView) view.findViewById(R.id.name_enquiryAdapter);
            SourceTv = (TextView) view.findViewById(R.id.source_enquiryAdapter);
            Destination1Tv = (TextView) view.findViewById(R.id.destination1_enquiryAdapter);
            Destination2Tv = (TextView) view.findViewById(R.id.destination2_enquiryAdapter);
            DateTv = (TextView) view.findViewById(R.id.date_enquiryAdapter);
            TimeTv = (TextView) view.findViewById(R.id.time_enquiryAdapter);
            StartSurveyBtn = (Button) view.findViewById(R.id.startSureveyBtn_enquiryAdapter);

            StartSurveyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentMainAdapter = new Intent(mContext, NewSurvey.class);
                    mContext.startActivity(intentMainAdapter);
                }
            });

        }
    }
}

package com.adarsh.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.AddRoom;
import com.adarsh.myapplication.activity.ManagePets;

import java.util.ArrayList;

public class RoomMasterAdapter extends RecyclerView.Adapter<RoomMasterAdapter.MyViewHolder> {

    ArrayList<String> RoomNameList,IdList;
    Context mContext;

    public RoomMasterAdapter(Context context, ArrayList<String> RoomNameList1,ArrayList<String>IdList1) {

        this.mContext = context;

        RoomNameList = new ArrayList<>();
        IdList = new ArrayList<>();

        this.RoomNameList = RoomNameList1;
        this.IdList = IdList1;

    }

    @Override
    public RoomMasterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.room_master_adapter, parent, false);

        return new RoomMasterAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RoomMasterAdapter.MyViewHolder holder, final int position) {

        holder.roomName.setText(RoomNameList.get(position));


        holder.editroombtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(mContext, AddRoom.class);
                mContext.startActivity(intent1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return RoomNameList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView roomName;
        ImageView editroombtn;

        public MyViewHolder(View view) {
            super(view);

            roomName = (TextView) view.findViewById(R.id.room_roomMasterAdapter);
            editroombtn = (ImageView)view.findViewById(R.id.editRoom_roomeMasterAdapter);

        }
    }
}

package com.adarsh.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.EditArticle;

import java.util.ArrayList;

public class ArticleMasterAdapter extends RecyclerView.Adapter<ArticleMasterAdapter.MyViewHolder> {

    ArrayList<String> NameList, VolumeList, VolumeUnitList, WeightList, WeightUnitList, IdList;
    Context mContext;

    public ArticleMasterAdapter(Context context, ArrayList<String> NameList1, ArrayList<String> VolumeList1, ArrayList<String> VolumeUnitList1,
                                ArrayList<String> WeightList1, ArrayList<String> WeightUnitList1, ArrayList<String> IdList1) {

        this.mContext = context;

        NameList = new ArrayList<>();
        VolumeList = new ArrayList<>();
        VolumeUnitList = new ArrayList<>();
        WeightList = new ArrayList<>();
        WeightUnitList = new ArrayList<>();
        IdList = new ArrayList<>();

        this.NameList = NameList1;
        this.VolumeList = VolumeList1;
        this.VolumeUnitList = VolumeUnitList1;
        this.WeightList = WeightList1;
        this.WeightUnitList = WeightUnitList1;
        this.IdList = IdList1;

    }

    @Override
    public ArticleMasterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_master_adapter, parent, false);

        return new ArticleMasterAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ArticleMasterAdapter.MyViewHolder holder, final int position) {

        holder.NameTv.setText(NameList.get(position));
        holder.VolumeTV.setText(VolumeList.get(position));
        holder.VolumeUnitTv.setText(VolumeUnitList.get(position));
        holder.WeightUnitTv.setText(WeightUnitList.get(position));
        holder.WeightTv.setText(WeightList.get(position));

        holder.EditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(mContext, EditArticle.class);
                intent1.putExtra("name", NameList.get(position));
                intent1.putExtra("volume", VolumeList.get(position));
                intent1.putExtra("volUnit", VolumeUnitList.get(position));
                intent1.putExtra("weightUnit", WeightUnitList.get(position));
                intent1.putExtra("weight", WeightList.get(position));
                intent1.putExtra("Id", IdList.get(position));
                mContext.startActivity(intent1);
                ((Activity) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return NameList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView NameTv, VolumeTV, VolumeUnitTv, WeightTv, WeightUnitTv;
        ImageView EditBtn;

        public MyViewHolder(View view) {
            super(view);

            NameTv = (TextView) view.findViewById(R.id.name_articleMasterAdapter);
            VolumeTV = (TextView) view.findViewById(R.id.volume_articleMasterAdapter);
            VolumeUnitTv = (TextView) view.findViewById(R.id.volumeUnit_articleMasterAdapter);
            WeightTv = (TextView) view.findViewById(R.id.weight_articleMasterAdapter);
            WeightUnitTv = (TextView) view.findViewById(R.id.weightUnit_articleMasterAdapter);
            EditBtn = (ImageView) view.findViewById(R.id.editArticle_articleMasterAdapter);
        }
    }
}

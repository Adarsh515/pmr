package com.adarsh.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.AddMovement;
import com.adarsh.myapplication.activity.ManagePets;

import java.util.ArrayList;

public class MovementMasterAdapter extends RecyclerView.Adapter<MovementMasterAdapter.MyViewHolder> {

    ArrayList<String> manageMovementList,IdList,MovementDescription;
    Context mContext;

    public MovementMasterAdapter(ArrayList<String> manageMovementList1, ArrayList<String> idList1,ArrayList<String> MovementDescription1, Context mContext) {
        manageMovementList = new ArrayList<>();
        IdList = new ArrayList<>();
        MovementDescription = new ArrayList<>();
        this.IdList = idList1;
        this.manageMovementList = manageMovementList1;
        this.MovementDescription = MovementDescription1;
        this.mContext = mContext;
    }

    @Override
    public MovementMasterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movement_master_adapter, parent, false);

        return new MovementMasterAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MovementMasterAdapter.MyViewHolder holder, final int position) {

        holder.movementName.setText(manageMovementList.get(position));
        holder.movementDescription.setText(MovementDescription.get(position));


        holder.editmovementbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(mContext, AddMovement.class);
                mContext.startActivity(intent1);
                ((Activity) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return manageMovementList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView movementName,movementDescription;
        ImageView editmovementbtn;

        public MyViewHolder(View view) {
            super(view);

            movementName = (TextView) view.findViewById(R.id.movementname_MovementMasterAdapter);
            movementDescription = (TextView) view.findViewById(R.id.description_MovementMasterAdapter);
            editmovementbtn = (ImageView)view.findViewById(R.id.editmovement_MovementMasterAdapter);

        }
    }
}

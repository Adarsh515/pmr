package com.adarsh.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.EditServices;

import java.util.ArrayList;

public class ServicesMasterAdapter extends RecyclerView.Adapter<ServicesMasterAdapter.MyViewHolder> {

    ArrayList<String> QuestionTitleList, MovementTypeList, QuestionForList, CheckListList, StatusList, IdList;
    Context mContext;

    public ServicesMasterAdapter(Context context, ArrayList<String> QuestionTitleList1, ArrayList<String> MovementTypeList1, ArrayList<String> QuestionForList1,
                                 ArrayList<String> CheckListList1, ArrayList<String> StatusList1, ArrayList<String> IdList1) {

        this.mContext = context;

        QuestionTitleList = new ArrayList<>();
        MovementTypeList = new ArrayList<>();
        QuestionForList = new ArrayList<>();
        CheckListList = new ArrayList<>();
        StatusList = new ArrayList<>();
        IdList = new ArrayList<>();

        this.QuestionTitleList = QuestionTitleList1;
        this.MovementTypeList = MovementTypeList1;
        this.QuestionForList = QuestionForList1;
        this.CheckListList = CheckListList1;
        this.StatusList = StatusList1;
        this.IdList = IdList1;

    }

    @Override
    public ServicesMasterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.services_master_adapter, parent, false);

        return new ServicesMasterAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ServicesMasterAdapter.MyViewHolder holder, final int position) {

        holder.QuestionTitleTV.setText(QuestionTitleList.get(position));
//        holder.MovementTypeTV.setText(MovementTypeList.get(position));
//        holder.QuestionForTV.setText(QuestionForList.get(position));
//        holder.StatusTV.setText(StatusList.get(position));
//        holder.CheckLisTV.setText(CheckListList.get(position));

        holder.EditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(mContext, EditServices.class);
//                intent1.putExtra("name",QuestionTitleList.get(position));
//                intent1.putExtra("volume",MovementTypeList.get(position));
//                intent1.putExtra("volUnit",QuestionForList.get(position));
//                intent1.putExtra("weightUnit",StatusList.get(position));
//                intent1.putExtra("weight",CheckListList.get(position));
//                intent1.putExtra("Id",IdList.get(position));
                mContext.startActivity(intent1);
                ((Activity) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return QuestionTitleList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView QuestionTitleTV, MovementTypeTV, QuestionForTV, CheckLisTV, StatusTV;
        ImageView EditBtn;

        public MyViewHolder(View view) {
            super(view);

            QuestionTitleTV = (TextView) view.findViewById(R.id.questionTitle_serviceMasterAdapter);
            MovementTypeTV = (TextView) view.findViewById(R.id.movementType_serviceMasterAdapter);
            QuestionForTV = (TextView) view.findViewById(R.id.questionFor_serviceMasterAdapter);
            CheckLisTV = (TextView) view.findViewById(R.id.checkList_serviceMasterAdapter);
            StatusTV = (TextView) view.findViewById(R.id.status_serviceMasterAdapter);
            EditBtn = (ImageView) view.findViewById(R.id.editService_serviceMasterAdapter);
        }
    }
}

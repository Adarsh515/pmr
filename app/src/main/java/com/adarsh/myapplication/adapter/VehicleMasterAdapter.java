package com.adarsh.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.VehicleMaster;

import java.util.ArrayList;

public class VehicleMasterAdapter extends RecyclerView.Adapter<VehicleMasterAdapter.MyViewHolder> {

    ArrayList<String> NameList, IdList;
    Context mContext;

    public VehicleMasterAdapter(Context context, ArrayList<String> NameList1, ArrayList<String> IdList1) {

        this.mContext = context;

        NameList = new ArrayList<>();
        IdList = new ArrayList<>();

        this.NameList = NameList1;
        this.IdList = IdList1;

    }

    @Override
    public VehicleMasterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapterforvehicle, parent, false);

        return new VehicleMasterAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VehicleMasterAdapter.MyViewHolder holder, final int position) {

        holder.Name_vehicle.setText(NameList.get(position));


        holder.edit_vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(mContext, VehicleMaster.class);
                mContext.startActivity(intent1);
                //((Activity) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return NameList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView Name_vehicle;
        ImageView edit_vehicle;

        public MyViewHolder(View view) {
            super(view);

            Name_vehicle = (TextView) view.findViewById(R.id.tvinputName_vehicle);
            edit_vehicle = (ImageView)view.findViewById(R.id.imgedit_vehicleadapter);

        }
    }
}

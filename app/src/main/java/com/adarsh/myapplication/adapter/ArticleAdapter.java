package com.adarsh.myapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adarsh.myapplication.R;

import java.util.ArrayList;


public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.MyViewHolder> {

    ArrayList<String> NameList, VolumeList, WeightList, IdList;
    Context mContext;

    public ArticleAdapter(Context context, ArrayList<String> NameList1, ArrayList<String> VolumeList1,
                          ArrayList<String> WeightList1, ArrayList<String> IdList1) {

        this.mContext = context;

        NameList = new ArrayList<>();
        VolumeList = new ArrayList<>();
        WeightList = new ArrayList<>();
        IdList = new ArrayList<>();

        this.NameList = NameList1;
        this.VolumeList = VolumeList1;
        this.WeightList = WeightList1;
        this.IdList = IdList1;

    }

    @Override
    public ArticleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_adapter, parent, false);

        return new ArticleAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ArticleAdapter.MyViewHolder holder, final int position) {

        holder.NameTv.setText(NameList.get(position));
        holder.VolumeTV.setText(VolumeList.get(position));
        holder.WeightTv.setText(WeightList.get(position));
    }

    @Override
    public int getItemCount() {
        return NameList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView NameTv, VolumeTV, WeightTv;

        public MyViewHolder(View view) {
            super(view);

            NameTv = (TextView) view.findViewById(R.id.articleName_Article);
            VolumeTV = (TextView) view.findViewById(R.id.articleVol_Article);
            WeightTv = (TextView) view.findViewById(R.id.articleWeight_Article);
        }
    }
}

package com.adarsh.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.ManagePets;

import java.util.ArrayList;

public class PetsSurveyAdapter extends RecyclerView.Adapter<PetsSurveyAdapter.MyViewHolder> {

    ArrayList<String> PetType,PetName,PetBreed,PetShipModel,PetAge,PetWeight,PetVolume,PetCageSize, IdList;
    Context mContext;

    public PetsSurveyAdapter(ArrayList<String> petType, ArrayList<String> petName, ArrayList<String> petBreed,
                             ArrayList<String> petShipModel, ArrayList<String> petAge, ArrayList<String> petWeight,
                             ArrayList<String> petVolume, ArrayList<String> petCageSize, ArrayList<String> idList,
                             Context mContext) {
        this.PetType = petType;
        this.PetName = petName;
        this.PetBreed = petBreed;
        this.PetShipModel = petShipModel;
        this.PetAge = petAge;
        this.PetWeight = petWeight;
        this.PetVolume = petVolume;
        this.PetCageSize = petCageSize;
        this.IdList = idList;
        this.mContext = mContext;
    }


    @Override
    public PetsSurveyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.petssurveyadapter, parent, false);

        return new PetsSurveyAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PetsSurveyAdapter.MyViewHolder holder, final int position) {

        holder.petType.setText(PetType.get(position));
        holder.name.setText(PetName.get(position));
        holder.breed.setText(PetBreed.get(position));
        holder.shipModel.setText(PetShipModel.get(position));
        holder.age.setText(PetAge.get(position));
        holder.weight.setText(PetWeight.get(position));
        holder.volume.setText(PetVolume.get(position));
        holder.cagesize.setText(PetCageSize.get(position));


        holder.editpetsSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(mContext, ManagePets.class);
                mContext.startActivity(intent1);
                //((Activity) mContext).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return PetType.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView petType,name,breed,shipModel,age,weight,volume,cagesize;
        ImageView editpetsSurvey;

        public MyViewHolder(View view) {
            super(view);

            petType = (TextView) view.findViewById(R.id.petType_PetSurveyAdapter);
            name = (TextView) view.findViewById(R.id.name_PetsSurveyAdapter);
            breed = (TextView) view.findViewById(R.id.breed_PetsSurveyAdapter);
            shipModel = (TextView) view.findViewById(R.id.shipModel_PetsSurveyAdapter);
            age = (TextView) view.findViewById(R.id.age_PetsSurveyAdapter);
            weight = (TextView) view.findViewById(R.id.weight_PetsSurveyAdapter);
            volume = (TextView) view.findViewById(R.id.volume_PetsSurveyAdapter);
            cagesize = (TextView) view.findViewById(R.id.cagesize_PetsSurveyAdapter);
            editpetsSurvey = (ImageView)view.findViewById(R.id.editService_PetsSurveyAdapter);

        }
    }
}

package com.adarsh.myapplication.app_helper;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {
    private static final String PREF_NAME = "edoe";
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String LoginRemember = "", MobileNo = "", EmailRemember = "", UserName = "", UserId = "", Fullname = "", ProfileImg = "", Pin = "", Password = "";
    Context context;

    public SessionManager(Context cntxt) {
        this.context = cntxt;
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public void Savepreferences(String key, String Value) {
        //  pref = PreferenceManager.getDefaultSharedPreferences(context);
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString(key, Value);
        editor.commit();
    }

    public String GetLoginRemember() {
        LoginRemember = (pref.getString("LoginRemember", ""));
        return LoginRemember;
    }

    public String GetEmailRemember() {
        EmailRemember = (pref.getString("EmailRemember", ""));
        return EmailRemember;
    }

    public String GetMobileNo() {
        MobileNo = (pref.getString("MobileNo", ""));
        return MobileNo;
    }

    public String GetUsername() {
        UserName = (pref.getString("UserName", ""));
        return UserName;
    }

    public String GetUserId() {
        UserId = (pref.getString("UserId", ""));
        return UserId;
    }

    public String GetFullname() {
        Fullname = (pref.getString("Fullname", ""));
        return Fullname;
    }

    public String GetProfileImg() {
        ProfileImg = (pref.getString("ProfileImg", ""));
        return ProfileImg;
    }

    public String GetPassword() {
        //Password = (pref.getString("password", ""));
        //return Password;
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString("Password", "");
    }

    public String GetPin() {
        Pin = (pref.getString("Pin", ""));
        return Pin;
    }
}


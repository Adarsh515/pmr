package com.adarsh.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Button;

import com.adarsh.myapplication.R;

import java.util.ArrayList;

public class EditServices extends AppCompatActivity {

    ArrayList<String> QuestionTitleList, MovementTypeList, QuestionForList, CheckListList, StatusList, IdList;

    Button SubmitBtn;

    @Override
    public void onBackPressed() {
        Intent intent2 = new Intent(EditServices.this, ServicesMaster.class);
        startActivity(intent2);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_services);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        SubmitBtn = (Button) findViewById(R.id.addBtn_addServices);

        SubmitBtn.setText("Submit");

//        QuestionTitleList=new ArrayList<>();
//        MovementTypeList=new ArrayList<>();
//        QuestionForList=new ArrayList<>();
//        CheckListList=new ArrayList<>();
//        StatusList=new ArrayList<>();
//        IdList=new ArrayList<>();
//
//        QuestionTitleList.clear();
//        MovementTypeList.clear();
//        QuestionForList.clear();
//        CheckListList.clear();
//        StatusList.clear();
//        IdList.clear();
    }


    @Override
    public boolean onSupportNavigateUp() {
        Intent intent2 = new Intent(EditServices.this, ServicesMaster.class);
        startActivity(intent2);
        finish();
        return true;
    }
}

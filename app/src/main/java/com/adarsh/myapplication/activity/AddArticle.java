package com.adarsh.myapplication.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.adarsh.myapplication.R;

import java.util.ArrayList;

public class AddArticle extends AppCompatActivity {

    Spinner WeightUnitSpinner, VolUnitSpinner;
    ArrayList<String> VolUnitList, WeightUnitList;
    String WeightUnitText = "", VolUnitText = "";
    Button AddArticleBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_article);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        AddArticleBtn = (Button) findViewById(R.id.addBtn_addArticle);

        WeightUnitSpinner = (Spinner) findViewById(R.id.weightUnit_addArticle);
        VolUnitSpinner = (Spinner) findViewById(R.id.volumeUnit_addArticle);

        VolUnitList = new ArrayList<>();
        WeightUnitList = new ArrayList<>();

        VolUnitList.clear();
        WeightUnitList.clear();

        VolUnitList.add("CFT");
        VolUnitList.add("CBM");
        ArrayAdapter adapterVolUnit = new ArrayAdapter(AddArticle.this, R.layout.single_textview, VolUnitList);
        VolUnitSpinner.setAdapter(adapterVolUnit);

        WeightUnitList.add("KG");
        WeightUnitList.add("LBS");
        ArrayAdapter adapterWeightUnit = new ArrayAdapter(AddArticle.this, R.layout.single_textview, WeightUnitList);
        WeightUnitSpinner.setAdapter(adapterWeightUnit);

        WeightUnitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                WeightUnitText = WeightUnitSpinner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        VolUnitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                VolUnitText = VolUnitSpinner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

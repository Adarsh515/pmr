package com.adarsh.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.adapter.MainAdapter;

import java.util.ArrayList;

public class SurveyRecords extends AppCompatActivity {

    RecyclerView recyclerViewMain;
    ArrayList<String> NameList, SourceList, DestinationList, DateList, TimeList, IdList;
    FloatingActionButton AddBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.survey_home);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AddBtn = (FloatingActionButton) findViewById(R.id.newSurveryBtn_main);
        recyclerViewMain = (RecyclerView) findViewById(R.id.recycler_main);
        recyclerViewMain.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));
        recyclerViewMain.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMain.setNestedScrollingEnabled(false);

        NameList = new ArrayList<>();
        DestinationList = new ArrayList<>();
        DateList = new ArrayList<>();
        SourceList = new ArrayList<>();
        TimeList = new ArrayList<>();
        IdList = new ArrayList<>();

        NameList.clear();
        DestinationList.clear();
        DateList.clear();
        SourceList.clear();
        TimeList.clear();
        IdList.clear();

        NameList.add("Adarsh");
        NameList.add("Abhishek");
        NameList.add("Surya");

        SourceList.add("Noida");
        SourceList.add("Delhi");
        SourceList.add("Gurgaon");

        DestinationList.add("Delhi");
        DestinationList.add("Gurgaon");
        DestinationList.add("Noida");

        DateList.add("16/11/2018");
        DateList.add("18/11/2018");
        DateList.add("14/11/2018");

        TimeList.add("19:55");
        TimeList.add("14:23");
        TimeList.add("16:40");

        MainAdapter adapter = new MainAdapter(SurveyRecords.this, NameList, IdList, SourceList, DestinationList, DateList, TimeList);
        recyclerViewMain.setAdapter(adapter);

        AddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentNew = new Intent(SurveyRecords.this, NewSurvey.class);
                startActivity(intentNew);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}


package com.adarsh.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.adapter.VehicleMasterAdapter;

import java.util.ArrayList;

public class VehicleMaster extends AppCompatActivity {

    RecyclerView RecyclerView_vehicle;
    ArrayList<String> VehicleNameList, IdList;
    FloatingActionButton AddVehicleBtn;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_master);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AddVehicleBtn = (FloatingActionButton) findViewById(R.id.addvehicleBtn_vehicle_master);
        RecyclerView_vehicle = (RecyclerView) findViewById(R.id.recycler_vehicle_master);
        RecyclerView_vehicle.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));
        RecyclerView_vehicle.setItemAnimator(new DefaultItemAnimator());
        RecyclerView_vehicle.setNestedScrollingEnabled(false);

        VehicleNameList = new ArrayList<>();
        IdList = new ArrayList<>();
        VehicleNameList.clear();
        IdList.clear();

        VehicleNameList.add("Yamaha FZs");
        VehicleNameList.add("TVS Apache");
        VehicleNameList.add("Maruti Suzuki Alto");
        VehicleNameList.add("BMW 800");

        IdList.add("1");
        IdList.add("2");

        VehicleMasterAdapter adapter = new VehicleMasterAdapter(VehicleMaster.this, VehicleNameList, IdList);
        RecyclerView_vehicle.setAdapter(adapter);

        AddVehicleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAdd = new Intent(VehicleMaster.this, AddVehicle.class);
                startActivity(intentAdd);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

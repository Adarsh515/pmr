package com.adarsh.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.adarsh.myapplication.R;

import java.util.ArrayList;

public class EditArticle extends AppCompatActivity {

    Spinner WeightUnitSpinner, VolUnitSpinner;
    ArrayList<String> VolUnitList, WeightUnitList;
    String WeightUnitText = "", VolUnitText = "", NameArtileTxt = "", DescriptionText = "", VolumeText = "", WeightText = "", Idtext = "";
    EditText WeigthED, VolumeED, DescriptionED, NameArticleED;
    Button SubmitBtn;

    @Override
    public void onBackPressed() {
        Intent intent2 = new Intent(EditArticle.this, ArticleMaster.class);
        startActivity(intent2);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_article);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        final Intent intent = getIntent();
        try {
            WeightUnitText = intent.getStringExtra("weightUnit");
            VolUnitText = intent.getStringExtra("volUnit");
            NameArtileTxt = intent.getStringExtra("name");
            VolumeText = intent.getStringExtra("volume");
            WeightText = intent.getStringExtra("weight");
            Idtext = intent.getStringExtra("Id");
        } catch (Exception e) {
            e.printStackTrace();
        }

        WeightUnitSpinner = (Spinner) findViewById(R.id.weightUnit_addArticle);
        VolUnitSpinner = (Spinner) findViewById(R.id.volumeUnit_addArticle);

        SubmitBtn = (Button) findViewById(R.id.addBtn_addArticle);
        SubmitBtn.setText("Submit");

        /*WeigthED = (EditText) findViewById(R.id.weight_addArticle);*/
        VolumeED = (EditText) findViewById(R.id.volume_addArticle);
        DescriptionED = (EditText) findViewById(R.id.description_addArticle);
        NameArticleED = (EditText) findViewById(R.id.articleName_addArticle);

        WeigthED.setText(WeightText);
        VolumeED.setText(VolumeText);
        NameArticleED.setText(NameArtileTxt);

        VolUnitList = new ArrayList<>();
        WeightUnitList = new ArrayList<>();

        VolUnitList.clear();
        WeightUnitList.clear();

        VolUnitList.add("CFT");
        VolUnitList.add("CBM");
        ArrayAdapter adapterVolUnit = new ArrayAdapter(EditArticle.this, R.layout.single_textview, VolUnitList);
        VolUnitSpinner.setAdapter(adapterVolUnit);

        WeightUnitList.add("KG");
        WeightUnitList.add("LBS");
        ArrayAdapter adapterWeightUnit = new ArrayAdapter(EditArticle.this, R.layout.single_textview, WeightUnitList);
        WeightUnitSpinner.setAdapter(adapterWeightUnit);

        WeightUnitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                WeightUnitText = WeightUnitSpinner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        VolUnitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                VolUnitText = VolUnitSpinner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(EditArticle.this, ArticleMaster.class);
                startActivity(intent2);
                finish();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent2 = new Intent(EditArticle.this, ArticleMaster.class);
        startActivity(intent2);
        finish();
        return true;
    }
}

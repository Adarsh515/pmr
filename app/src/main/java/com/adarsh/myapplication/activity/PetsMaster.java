package com.adarsh.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.adapter.PetsMasterAdapter;

import java.util.ArrayList;

public class PetsMaster extends AppCompatActivity {

    RecyclerView RecyclerView_pets;
    ArrayList<String> PetsNameList, IdList;
    FloatingActionButton AddPetsBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_for_pets);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AddPetsBtn = (FloatingActionButton) findViewById(R.id.addPetsBtn_pets_master);
        RecyclerView_pets = (RecyclerView) findViewById(R.id.recycler_pets_master);
        RecyclerView_pets.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));
        RecyclerView_pets.setItemAnimator(new DefaultItemAnimator());
        RecyclerView_pets.setNestedScrollingEnabled(false);

        PetsNameList = new ArrayList<>();
        IdList = new ArrayList<>();
        PetsNameList.clear();
        IdList.clear();

        PetsNameList.add("Tuffy");
        PetsNameList.add("Sherry");

        IdList.add("1");
        IdList.add("2");
        PetsMasterAdapter adapter = new PetsMasterAdapter(PetsMaster.this, PetsNameList, IdList);
        RecyclerView_pets.setAdapter(adapter);

        AddPetsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAdd = new Intent(PetsMaster.this, ManagePets.class);
                startActivity(intentAdd);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

package com.adarsh.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.app_helper.SessionManager;

public class Login extends AppCompatActivity {

    SessionManager sessionManager;
    Button LoginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        sessionManager = new SessionManager(this);

        LoginBtn = (Button) findViewById(R.id.loginBtn_login);

        LoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLogin = new Intent(Login.this, MainActivity.class);
                startActivity(intentLogin);
                finish();
            }
        });


    }
}

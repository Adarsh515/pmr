package com.adarsh.myapplication.activity;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.adapter.MovementMasterAdapter;
import com.adarsh.myapplication.adapter.RoomMasterAdapter;

import java.util.ArrayList;

public class MovementMaster extends AppCompatActivity {

    RecyclerView recycler_movement_master;
    FloatingActionButton addBtn_MovementMaster;
    ArrayList<String> manageMovementList,IdList,MovementDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movement_master);

        recycler_movement_master = (RecyclerView)findViewById(R.id.recycler_movement_master);
        addBtn_MovementMaster = (FloatingActionButton)findViewById(R.id.addmovementBtn_MovementMaster);
        recycler_movement_master.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));
        recycler_movement_master.setItemAnimator(new DefaultItemAnimator());
        recycler_movement_master.setNestedScrollingEnabled(false);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        manageMovementList = new ArrayList<>();
        MovementDescription = new ArrayList<>();
        IdList = new ArrayList<>();
        IdList.clear();
        manageMovementList.clear();
        manageMovementList.add("Manage Movement List");
        MovementDescription.add("Movement Description");
        IdList.add("1");
        IdList.add("2");

        MovementMasterAdapter adapter = new MovementMasterAdapter(manageMovementList,IdList,MovementDescription,MovementMaster.this);
        recycler_movement_master.setAdapter(adapter);

        addBtn_MovementMaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentmovement = new Intent(MovementMaster.this, AddMovement.class);
                startActivity(intentmovement);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

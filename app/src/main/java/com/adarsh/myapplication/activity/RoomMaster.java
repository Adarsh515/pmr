package com.adarsh.myapplication.activity;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.adapter.RoomMasterAdapter;

import java.util.ArrayList;

public class RoomMaster extends AppCompatActivity {

    RecyclerView recyclerViewRoomMaster;
    FloatingActionButton AddRoomBtn;
    ArrayList<String> RoomNameList,IdList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_master);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        AddRoomBtn = (FloatingActionButton) findViewById(R.id.addroomBtn_roomMaster);
        recyclerViewRoomMaster = (RecyclerView) findViewById(R.id.recycler_room_master);
        recyclerViewRoomMaster.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));
        recyclerViewRoomMaster.setItemAnimator(new DefaultItemAnimator());
        recyclerViewRoomMaster.setNestedScrollingEnabled(false);

        RoomNameList = new ArrayList<>();
        IdList = new ArrayList<>();
        IdList.clear();
        RoomNameList.clear();
        RoomNameList.add("Room Name List");
        IdList.add("1");
        IdList.add("2");


        RoomMasterAdapter adapter = new RoomMasterAdapter(RoomMaster.this, RoomNameList,IdList);
        recyclerViewRoomMaster.setAdapter(adapter);

        AddRoomBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentroom = new Intent(RoomMaster.this, AddRoom.class);
                startActivity(intentroom);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

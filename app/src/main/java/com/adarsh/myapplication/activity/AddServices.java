package com.adarsh.myapplication.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.adarsh.myapplication.R;

import java.util.ArrayList;

public class AddServices extends AppCompatActivity {

    ArrayList<String> QuestionTitleList, MovementTypeList, QuestionForList, CheckListList, StatusList, IdList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_services);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

//        QuestionTitleList=new ArrayList<>();
//        MovementTypeList=new ArrayList<>();
//        QuestionForList=new ArrayList<>();
//        CheckListList=new ArrayList<>();
//        StatusList=new ArrayList<>();
//        IdList=new ArrayList<>();
//
//        QuestionTitleList.clear();
//        MovementTypeList.clear();
//        QuestionForList.clear();
//        CheckListList.clear();
//        StatusList.clear();
//        IdList.clear();
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

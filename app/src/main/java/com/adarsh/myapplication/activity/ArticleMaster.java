package com.adarsh.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.adapter.ArticleMasterAdapter;

import java.util.ArrayList;

public class ArticleMaster extends AppCompatActivity {

    RecyclerView recyclerViewArticleMaster;
    FloatingActionButton AddArticleBtn;

    ArrayList<String> NameList, VolumeList, VolumeUnitList, WeightList, WeightUnitList, IdList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_master);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AddArticleBtn = (FloatingActionButton) findViewById(R.id.addArticleBtn_article_master);
        recyclerViewArticleMaster = (RecyclerView) findViewById(R.id.recycler_article_master);
        recyclerViewArticleMaster.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));
        recyclerViewArticleMaster.setItemAnimator(new DefaultItemAnimator());
        recyclerViewArticleMaster.setNestedScrollingEnabled(false);

        NameList = new ArrayList<>();
        VolumeList = new ArrayList<>();
        VolumeUnitList = new ArrayList<>();
        WeightList = new ArrayList<>();
        WeightUnitList = new ArrayList<>();
        IdList = new ArrayList<>();

        NameList.clear();
        VolumeList.clear();
        VolumeUnitList.clear();
        WeightList.clear();
        WeightUnitList.clear();
        IdList.clear();

        NameList.add("Crockery");
        NameList.add("Bed");

        VolumeList.add("200.00");
        VolumeList.add("50.00");

        VolumeUnitList.add("CBM");
        VolumeUnitList.add("CBM");

        WeightList.add("45");
        WeightList.add("50");

        WeightUnitList.add("KG");
        WeightUnitList.add("KG");

        IdList.add("1");
        IdList.add("2");

        ArticleMasterAdapter adapter = new ArticleMasterAdapter(ArticleMaster.this, NameList, VolumeList, VolumeUnitList, WeightList,
                WeightUnitList, IdList);
        recyclerViewArticleMaster.setAdapter(adapter);

        AddArticleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAdd = new Intent(ArticleMaster.this, AddArticle.class);
                startActivity(intentAdd);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

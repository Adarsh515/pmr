package com.adarsh.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.adapter.ServicesMasterAdapter;

import java.util.ArrayList;

public class ServicesMaster extends AppCompatActivity {

    RecyclerView recyclerViewServicesMaster;
    FloatingActionButton AddServicesBtn;

    ArrayList<String> QuestionTitleList, MovementTypeList, QuestionForList, CheckListList, StatusList, IdList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.services_master);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AddServicesBtn = (FloatingActionButton) findViewById(R.id.addServiceBtn_ServiceMaster);
        recyclerViewServicesMaster = (RecyclerView) findViewById(R.id.recycler_service_master);
        recyclerViewServicesMaster.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1,
                GridLayoutManager.VERTICAL, false));
        recyclerViewServicesMaster.setItemAnimator(new DefaultItemAnimator());
        recyclerViewServicesMaster.setNestedScrollingEnabled(false);

        QuestionTitleList = new ArrayList<>();
        MovementTypeList = new ArrayList<>();
        QuestionForList = new ArrayList<>();
        CheckListList = new ArrayList<>();
        StatusList = new ArrayList<>();
        IdList = new ArrayList<>();

        QuestionTitleList.clear();
        MovementTypeList.clear();
        QuestionForList.clear();
        CheckListList.clear();
        StatusList.clear();
        IdList.clear();

        QuestionTitleList.add("Asset list given in case of entire office movement");
        QuestionTitleList.add("No of Days Moved to be Done in");
        QuestionTitleList.add("Total No of Employee to be Moved");
        QuestionTitleList.add("No of Phase Required in Shifting");

        //QuestionTitleList,MovementTypeList,QuestionForList,CheckListList,StatusList,IdList;

        ServicesMasterAdapter adapter = new ServicesMasterAdapter(ServicesMaster.this, QuestionTitleList, MovementTypeList, QuestionForList, CheckListList,
                StatusList, IdList);
        recyclerViewServicesMaster.setAdapter(adapter);

        AddServicesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentService = new Intent(ServicesMaster.this, AddServices.class);
                startActivity(intentService);
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

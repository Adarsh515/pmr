package com.adarsh.myapplication.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.NewSurvey;
import com.adarsh.myapplication.activity.VehicleSurvey;
import com.adarsh.myapplication.adapter.VehicleMasterAdapter;

import java.util.ArrayList;

public class Vehicle extends Fragment {

    Button NextBtn;
    RecyclerView RecyclerView_vehicle;
    ArrayList<String> VehicleNameList, IdList;
    FloatingActionButton AddVehicleBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.vehicle, container, false);

        NextBtn = (Button) rootView.findViewById(R.id.nextBtn_vehicle_fragment);
        AddVehicleBtn = (FloatingActionButton) rootView.findViewById(R.id.addVehicleBtn_vehicle_fragment);
        RecyclerView_vehicle = (RecyclerView) rootView.findViewById(R.id.recycler_vehicle_fragment);
        RecyclerView_vehicle.setLayoutManager(new GridLayoutManager(getActivity(), 1,
                GridLayoutManager.VERTICAL, false));
        RecyclerView_vehicle.setItemAnimator(new DefaultItemAnimator());
        RecyclerView_vehicle.setNestedScrollingEnabled(false);

        VehicleNameList = new ArrayList<>();
        IdList = new ArrayList<>();
        VehicleNameList.clear();
        IdList.clear();

        VehicleNameList.add("Yamaha FZs");
        VehicleNameList.add("TVS Apache");
        VehicleNameList.add("Maruti Suzuki Alto");
        VehicleNameList.add("BMW 800");

        IdList.add("1");
        IdList.add("2");

        VehicleMasterAdapter adapter = new VehicleMasterAdapter(getActivity(), VehicleNameList, IdList);
        RecyclerView_vehicle.setAdapter(adapter);

        AddVehicleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAdd = new Intent(getActivity(), VehicleSurvey.class);
                startActivity(intentAdd);
            }
        });

        NextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewSurvey.viewPager.setCurrentItem(4, true);
            }
        });

        return rootView;
    }
}

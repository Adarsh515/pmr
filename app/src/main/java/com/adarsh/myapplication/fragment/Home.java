package com.adarsh.myapplication.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.ArticleMaster;
import com.adarsh.myapplication.activity.MovementMaster;
import com.adarsh.myapplication.activity.NewSurvey;
import com.adarsh.myapplication.activity.PetsMaster;
import com.adarsh.myapplication.activity.RoomMaster;
import com.adarsh.myapplication.activity.ServicesMaster;
import com.adarsh.myapplication.activity.VehicleMaster;

public class Home extends Fragment {

    LinearLayout QuestionnaireLL, ArticleLL, MovementLL, VehicleLL, PetsLL, RoomsLL;
    Button StartSurveyBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.home, container, false);

        QuestionnaireLL = (LinearLayout) rootView.findViewById(R.id.questionnaire_home);
        ArticleLL = (LinearLayout) rootView.findViewById(R.id.article_home);
        MovementLL = (LinearLayout) rootView.findViewById(R.id.movement_home);
        VehicleLL = (LinearLayout) rootView.findViewById(R.id.vehicle_home);
        PetsLL = (LinearLayout) rootView.findViewById(R.id.pets_home);
        RoomsLL = (LinearLayout) rootView.findViewById(R.id.rooms_home);
        StartSurveyBtn = (Button) rootView.findViewById(R.id.startSurveyBtn_home);

        QuestionnaireLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentQues = new Intent(getActivity(), ServicesMaster.class);
                startActivity(intentQues);
            }
        });

        ArticleLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentArticle = new Intent(getActivity(), ArticleMaster.class);
                startActivity(intentArticle);
            }
        });

        MovementLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentMovement = new Intent(getActivity(), MovementMaster.class);
                startActivity(intentMovement);
            }
        });

        VehicleLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentVehicle = new Intent(getActivity(), VehicleMaster.class);
                startActivity(intentVehicle);
            }
        });

        PetsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentPets = new Intent(getActivity(), PetsMaster.class);
                startActivity(intentPets);
            }
        });

        RoomsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRooms = new Intent(getActivity(), RoomMaster.class);
                startActivity(intentRooms);
            }
        });

        StartSurveyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentStartSurevy = new Intent(getActivity(), NewSurvey.class);
                startActivity(intentStartSurevy);
            }
        });

        return rootView;
    }
}
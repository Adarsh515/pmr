package com.adarsh.myapplication.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.NewSurvey;

public class Services extends Fragment {

    Button NextBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.services, container, false);

        NextBtn = (Button) rootView.findViewById(R.id.nextBtn_Services);

        NextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewSurvey.viewPager.setCurrentItem(3, true);
            }
        });
        return rootView;
    }
}

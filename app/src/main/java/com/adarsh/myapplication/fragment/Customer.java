package com.adarsh.myapplication.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.NewSurvey;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class Customer extends Fragment {

    Spinner EnquiryTypeSpinner, ServiceTypeSpinner, GoodsTypeSpinner, StatusSpinner, FrequencySpinner, StorageModeSpinner;
    ArrayList<String> EnquiryTypeList, ServiceTypeList, GoodsTypeList, StatusList, FrequencyList, StorageModeList;
    String EnquiryType = "", ServiceTypeText = "", GoodsTypeText = "", StatusText = "", StorageModeText = "", FrequencyText = "";
    ImageView ArrowCustomerBtn, ArrowSurveyBtn, ArrowOriginAddressBtn, ArrowDestinationAddressBtn;
    ImageView ArrowStorageBtn, ArrowMoveBtn;
    int CustomerHide = 0, SurveyHide = 0, OriginAddressHide = 0, DestinationAddressHide = 0, StorageHide = 0, MoveHide = 0;
    LinearLayout CustomerLayout, SurveyLayout, OriginAddressLayout, DestinationAddressLayout;
    LinearLayout StorageLayout, MoveLayout;

    TextView SurveyDateTV, StartTimeTv, EndTimeTv;
    EditText AddressED;

    Calendar mcurrentDate = Calendar.getInstance();
    int mYear = mcurrentDate.get(Calendar.YEAR);
    int mMonth = mcurrentDate.get(Calendar.MONTH);
    int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
    int hour, minute;

    Button SaveBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.customer, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        EnquiryTypeSpinner = (Spinner) rootView.findViewById(R.id.enquiryType_Customer);
        ServiceTypeSpinner = (Spinner) rootView.findViewById(R.id.serviceType_Customer);
        GoodsTypeSpinner = (Spinner) rootView.findViewById(R.id.goodsType_Customer);
        StatusSpinner = (Spinner) rootView.findViewById(R.id.status_Customer);
        FrequencySpinner = (Spinner) rootView.findViewById(R.id.frequency_Customer);
        StorageModeSpinner = (Spinner) rootView.findViewById(R.id.storageMode_Customer);

        ArrowCustomerBtn = (ImageView) rootView.findViewById(R.id.arrowCustomerDetails_Customer);
        ArrowSurveyBtn = (ImageView) rootView.findViewById(R.id.arrowSurveyDetails_Customer);
        ArrowOriginAddressBtn = (ImageView) rootView.findViewById(R.id.arrowOriginAddress_Customer);
        ArrowDestinationAddressBtn = (ImageView) rootView.findViewById(R.id.arrowDestinationAddress_Customer);
        ArrowStorageBtn = (ImageView) rootView.findViewById(R.id.arrowStorageDetails_Customer);
        ArrowMoveBtn = (ImageView) rootView.findViewById(R.id.arrowMoveDetails_Customer);

        AddressED = (EditText) rootView.findViewById(R.id.address_Customer);

        SaveBtn = (Button) rootView.findViewById(R.id.saveBtn_Customer);

        SurveyDateTV = (TextView) rootView.findViewById(R.id.surveyDate_Customer);
        StartTimeTv = (TextView) rootView.findViewById(R.id.surveyStartTime_Customer);
        EndTimeTv = (TextView) rootView.findViewById(R.id.surveyEndTime_Customer);

        CustomerLayout = (LinearLayout) rootView.findViewById(R.id.linearCustomerDetails_Customer);
        SurveyLayout = (LinearLayout) rootView.findViewById(R.id.linearSurveyDetails_Customer);
        OriginAddressLayout = (LinearLayout) rootView.findViewById(R.id.linearaddressOrigin_Customer);
        DestinationAddressLayout = (LinearLayout) rootView.findViewById(R.id.linearDestinationAddress_Customer);
        StorageLayout = (LinearLayout) rootView.findViewById(R.id.linearStorageDetails_Customer);
        MoveLayout = (LinearLayout) rootView.findViewById(R.id.linearMoveDetails_Customer);

        EnquiryTypeList = new ArrayList<>();
        ServiceTypeList = new ArrayList<>();
        GoodsTypeList = new ArrayList<>();
        StatusList = new ArrayList<>();
        FrequencyList = new ArrayList<>();
        StorageModeList = new ArrayList<>();

        ServiceTypeList.clear();
        GoodsTypeList.clear();
        StatusList.clear();
        EnquiryTypeList.clear();
        FrequencyList.clear();
        StorageModeList.clear();

        EnquiryTypeList.add("Individual");
        EnquiryTypeList.add("Corporate");
        EnquiryTypeList.add("Agent");
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.single_textview, EnquiryTypeList);
        EnquiryTypeSpinner.setAdapter(adapter);

        ServiceTypeList.add("Domestic Moving");
        ServiceTypeList.add("International Moving");
        ArrayAdapter adapterService = new ArrayAdapter(getActivity(), R.layout.single_textview, ServiceTypeList);
        ServiceTypeSpinner.setAdapter(adapterService);

        GoodsTypeList.add("Household Goods");
        GoodsTypeList.add("Office Goods");
        ArrayAdapter adapterGoods = new ArrayAdapter(getActivity(), R.layout.single_textview, GoodsTypeList);
        GoodsTypeSpinner.setAdapter(adapterGoods);

        StatusList.add("Active");
        StatusList.add("Done");
        StatusList.add("Canceled");
        StatusList.add("In Process");
        ArrayAdapter adapterStatusList = new ArrayAdapter(getActivity(), R.layout.single_textview, StatusList);
        StatusSpinner.setAdapter(adapterStatusList);

        FrequencyList.add("Select");
        ArrayAdapter adapterFrequency = new ArrayAdapter(getActivity(), R.layout.single_textview, FrequencyList);
        FrequencySpinner.setAdapter(adapterFrequency);

        StorageModeList.add("Select");
        ArrayAdapter adapterStorageMode = new ArrayAdapter(getActivity(), R.layout.single_textview, StorageModeList);
        StorageModeSpinner.setAdapter(adapterStorageMode);

        EnquiryTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EnquiryType = EnquiryTypeSpinner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ServiceTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ServiceTypeText = ServiceTypeSpinner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        GoodsTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                GoodsTypeText = GoodsTypeSpinner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        StatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StatusText = StatusSpinner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        StorageModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StorageModeText = StorageModeSpinner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        FrequencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FrequencyText = FrequencySpinner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrowCustomerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CustomerHide == 0) {
                    CustomerHide = 1;
                    AddressED.setVisibility(View.GONE);
                    CustomerLayout.setVisibility(View.GONE);
                    ArrowCustomerBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.down_arrow_white));
                } else {
                    CustomerHide = 0;
                    AddressED.setVisibility(View.VISIBLE);
                    CustomerLayout.setVisibility(View.VISIBLE);
                    ArrowCustomerBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.up_arrow_white));
                }
            }
        });

        ArrowSurveyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SurveyHide == 0) {
                    SurveyHide = 1;
                    SurveyLayout.setVisibility(View.GONE);
                    ArrowSurveyBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.down_arrow_white));
                } else {
                    SurveyHide = 0;
                    SurveyLayout.setVisibility(View.VISIBLE);
                    ArrowSurveyBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.up_arrow_white));
                }
            }
        });

        ArrowOriginAddressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OriginAddressHide == 0) {
                    OriginAddressHide = 1;
                    OriginAddressLayout.setVisibility(View.GONE);
                    ArrowOriginAddressBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.down_arrow_white));
                } else {
                    OriginAddressHide = 0;
                    OriginAddressLayout.setVisibility(View.VISIBLE);
                    ArrowOriginAddressBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.up_arrow_white));
                }
            }
        });

        ArrowDestinationAddressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DestinationAddressHide == 0) {
                    DestinationAddressHide = 1;
                    DestinationAddressLayout.setVisibility(View.GONE);
                    ArrowDestinationAddressBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.down_arrow_white));
                } else {
                    DestinationAddressHide = 0;
                    DestinationAddressLayout.setVisibility(View.VISIBLE);
                    ArrowDestinationAddressBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.up_arrow_white));
                }
            }
        });

        ArrowStorageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StorageHide == 0) {
                    StorageHide = 1;
                    StorageLayout.setVisibility(View.GONE);
                    ArrowStorageBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.down_arrow_white));
                } else {
                    StorageHide = 0;
                    StorageLayout.setVisibility(View.VISIBLE);
                    ArrowStorageBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.up_arrow_white));
                }
            }
        });

        ArrowMoveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MoveHide == 0) {
                    MoveHide = 1;
                    MoveLayout.setVisibility(View.GONE);
                    ArrowMoveBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.down_arrow_white));
                } else {
                    MoveHide = 0;
                    MoveLayout.setVisibility(View.VISIBLE);
                    ArrowMoveBtn.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.up_arrow_white));
                }
            }
        });

        SurveyDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] DATE = {""};

                DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), R.style.TimePickerTheme, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        Calendar myCalendar = Calendar.getInstance();
                        myCalendar.set(Calendar.YEAR, selectedyear);
                        myCalendar.set(Calendar.MONTH, selectedmonth);
                        myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);
                        int day = myCalendar.get(Calendar.DAY_OF_WEEK);
                        String myFormat = "d MMM yyyy"; //Change as you need
                        String Format2 = "dd/MM/yyyy";
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
                        SimpleDateFormat sdf1 = new SimpleDateFormat(Format2, Locale.ENGLISH);
                        DATE[0] = sdf1.format(myCalendar.getTime());
                        SurveyDateTV.setText(DATE[0]);
                        int mDay1 = selectedday;
                        int mMonth1 = selectedmonth;
                        int mYear1 = selectedyear;
                    }
                }, mYear, mMonth, mDay);
                //mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });

        StartTimeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String[] TIME = {""};
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), R.style.TimePickerTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        if (selectedHour == 0) {
                            TIME[0] = selectedHour + "0" + ":" + selectedMinute;
                        } else if (selectedMinute == 0) {
                            TIME[0] = selectedHour + ":" + selectedMinute + "0";
                        } else if (selectedMinute == 0 && selectedHour == 0) {
                            TIME[0] = selectedHour + "0" + ":" + selectedMinute + "0";
                        } else {
                            TIME[0] = selectedHour + ":" + selectedMinute;
                        }

                        StartTimeTv.setText(TIME[0]);
                        //TimeTv.setTextColor(getResources().getColor(R.color.black));
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        EndTimeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] TIME = {""};
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), R.style.TimePickerTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        if (selectedHour == 0) {
                            TIME[0] = selectedHour + "0" + ":" + selectedMinute;
                        } else if (selectedMinute == 0) {
                            TIME[0] = selectedHour + ":" + selectedMinute + "0";
                        } else if (selectedMinute == 0 && selectedHour == 0) {
                            TIME[0] = selectedHour + "0" + ":" + selectedMinute + "0";
                        } else {
                            TIME[0] = selectedHour + ":" + selectedMinute;
                        }
                        EndTimeTv.setText(TIME[0]);
                        //TimeTv.setTextColor(getResources().getColor(R.color.black));
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        SaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewSurvey.viewPager.setCurrentItem(1, true);
            }
        });

        return rootView;
    }

    public String DatePicker() {
        final String[] DateString = {""};
        DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), R.style.TimePickerTheme, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                Calendar myCalendar = Calendar.getInstance();
                myCalendar.set(Calendar.YEAR, selectedyear);
                myCalendar.set(Calendar.MONTH, selectedmonth);
                myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);
                int day = myCalendar.get(Calendar.DAY_OF_WEEK);
                String myFormat = "d MMM yyyy"; //Change as you need
                String Format2 = "dd/MM/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
                SimpleDateFormat sdf1 = new SimpleDateFormat(Format2, Locale.ENGLISH);
                DateString[0] = sdf1.format(myCalendar.getTime());

                int mDay1 = selectedday;
                int mMonth1 = selectedmonth;
                int mYear1 = selectedyear;
            }
        }, mYear, mMonth, mDay);
        //mDatePicker.setTitle("Select date");
        mDatePicker.show();

        return DateString[0];
    }

    public String TimePicker() {
        TimePickerDialog mTimePicker;
        final String[] AppointmentTime = {""};
        mTimePicker = new TimePickerDialog(getActivity(), R.style.TimePickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                AppointmentTime[0] = selectedHour + ":" + selectedMinute;
                //TimeTv.setTextColor(getResources().getColor(R.color.black));
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

        return AppointmentTime[0];
    }
}



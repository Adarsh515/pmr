package com.adarsh.myapplication.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.adapter.EnquiryAdapter;

import java.util.ArrayList;

public class EnquiryHome extends Fragment {

    RecyclerView recyclerViewMain;
    ArrayList<String> NameList, SourceList, DestinationList, DateList, TimeList, IdList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.enquiry_home, container, false);
        recyclerViewMain = (RecyclerView) rootView.findViewById(R.id.recycler_main);
        recyclerViewMain.setLayoutManager(new GridLayoutManager(getActivity(), 1,
                GridLayoutManager.VERTICAL, false));
        recyclerViewMain.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMain.setNestedScrollingEnabled(false);

        NameList = new ArrayList<>();
        DestinationList = new ArrayList<>();
        DateList = new ArrayList<>();
        SourceList = new ArrayList<>();
        TimeList = new ArrayList<>();
        IdList = new ArrayList<>();

        NameList.clear();
        DestinationList.clear();
        DateList.clear();
        SourceList.clear();
        TimeList.clear();
        IdList.clear();

        NameList.add("Adarsh");
        NameList.add("Abhishek");
        NameList.add("Surya");

        SourceList.add("Noida");
        SourceList.add("Delhi");
        SourceList.add("Gurgaon");

        DestinationList.add("Delhi");
        DestinationList.add("Gurgaon");
        DestinationList.add("Noida");

        DateList.add("16/11/2018");
        DateList.add("18/11/2018");
        DateList.add("14/11/2018");

        TimeList.add("19:55");
        TimeList.add("14:23");
        TimeList.add("16:40");

        NameList.add("Adarsh");
        NameList.add("Abhishek");
        NameList.add("Surya");

        SourceList.add("Noida");
        SourceList.add("Delhi");
        SourceList.add("Gurgaon");

        DestinationList.add("Delhi");
        DestinationList.add("Gurgaon");
        DestinationList.add("Noida");

        DateList.add("16/11/2018");
        DateList.add("18/11/2018");
        DateList.add("14/11/2018");

        TimeList.add("19:55");
        TimeList.add("14:23");
        TimeList.add("16:40");

        EnquiryAdapter adapter = new EnquiryAdapter(getActivity(), NameList, IdList, SourceList, DestinationList, DateList, TimeList);
        recyclerViewMain.setAdapter(adapter);

        return rootView;
    }
}
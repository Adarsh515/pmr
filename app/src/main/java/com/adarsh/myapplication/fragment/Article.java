package com.adarsh.myapplication.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.NewSurvey;
import com.adarsh.myapplication.adapter.ArticleAdapter;

import java.util.ArrayList;

public class Article extends Fragment {

    RecyclerView recyclerViewArticleMaster;
    ArrayList<String> NameList, VolumeList, WeightList, IdList;

    Button NextBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.article, container, false);

        NextBtn = (Button) rootView.findViewById(R.id.nextBtn_article);
        recyclerViewArticleMaster = (RecyclerView) rootView.findViewById(R.id.recycler_article);
        recyclerViewArticleMaster.setLayoutManager(new GridLayoutManager(getActivity(), 1,
                GridLayoutManager.VERTICAL, false));
        recyclerViewArticleMaster.setItemAnimator(new DefaultItemAnimator());
        recyclerViewArticleMaster.setNestedScrollingEnabled(false);

        NameList = new ArrayList<>();
        VolumeList = new ArrayList<>();
        WeightList = new ArrayList<>();
        IdList = new ArrayList<>();

        NameList.clear();
        VolumeList.clear();
        WeightList.clear();
        IdList.clear();

        NameList.add("Crockery");
        NameList.add("Bed");

        VolumeList.add("200.00 CFT");
        VolumeList.add("50.00 CFT");

        WeightList.add("45 KG");
        WeightList.add("50 KG");

        IdList.add("1");
        IdList.add("2");

        NameList.add("AC");
        NameList.add("Book Shelf");

        VolumeList.add("100.00 CFT");
        VolumeList.add("150.00 CFT");

        WeightList.add("15 KG");
        WeightList.add("30 KG");

        IdList.add("3");
        IdList.add("4");

        ArticleAdapter articleAdapter = new ArticleAdapter(getActivity(), NameList, VolumeList, WeightList, IdList);
        recyclerViewArticleMaster.setAdapter(articleAdapter);

        NextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewSurvey.viewPager.setCurrentItem(2, true);
            }
        });

        return rootView;
    }
}
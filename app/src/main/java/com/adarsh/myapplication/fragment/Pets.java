package com.adarsh.myapplication.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.adarsh.myapplication.R;
import com.adarsh.myapplication.activity.PetsSurvey;
import com.adarsh.myapplication.adapter.PetsMasterAdapter;

import java.util.ArrayList;

public class Pets extends Fragment {

    Button SubmitBtn;
    RecyclerView RecyclerView_pets;
    ArrayList<String> PetsNameList, IdList;
    FloatingActionButton AddPetsBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.pets, container, false);

        SubmitBtn = (Button) rootView.findViewById(R.id.submitBtn_pets_fragment);
        AddPetsBtn = (FloatingActionButton) rootView.findViewById(R.id.addPetsBtn_pets_fragment);
        RecyclerView_pets = (RecyclerView) rootView.findViewById(R.id.recycler_pets_fragment);
        RecyclerView_pets.setLayoutManager(new GridLayoutManager(getActivity(), 1,
                GridLayoutManager.VERTICAL, false));
        RecyclerView_pets.setItemAnimator(new DefaultItemAnimator());
        RecyclerView_pets.setNestedScrollingEnabled(false);

        PetsNameList = new ArrayList<>();
        IdList = new ArrayList<>();
        PetsNameList.clear();
        IdList.clear();

        PetsNameList.add("Tuffy");
        PetsNameList.add("Sherry");

        IdList.add("1");
        IdList.add("2");
        PetsMasterAdapter adapter = new PetsMasterAdapter(getActivity(), PetsNameList, IdList);
        RecyclerView_pets.setAdapter(adapter);

        AddPetsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAdd = new Intent(getActivity(), PetsSurvey.class);
                startActivity(intentAdd);
            }
        });

        SubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return rootView;
    }
}
